tomcatPID=$(ps -ef | grep tomcat | grep -v grep | grep -v restart | awk '{print $2}')
if $jobplanet  = 'true'
then 
  echo "Attempting to find process id for Tomcat . . ."
  echo $tomcatPID
  else
  echo " NO Deployment for Jobplant"
fi  
if [ "$tomcatPID" == "" ]
then
    echo "Tomcat does not appear to be running . . ."
else
    echo "Killing Tomcat using process id of $tomcatPID . . ."
    sudo kill -9 $tomcatPID
    echo "Waiting for process $tomcatPID to end . . ."
    sudo  ps -ef | grep $tomcatPID
    echo "Process $tomcatPID has ended . . ."
fi


2
if $jobplanet  = 'true'
then 
cd /home/centos/apache-tomcat-9.0.19/webapps/
sudo tar -cvf ${BUILD_NUMBER}_aws_jobplanet.tar *
sudo mv ${BUILD_NUMBER}_aws_jobplanet.tar /data/backup/aws_jobplanet  
echo "Backup Is Sucessfull"
else
echo "BACK IS TAKEN SUCESSFULLY FOR JOBPLANET"
fi


3
if $jobplanet  = 'true'
then
cd /home/centos/apache-tomcat-9.0.19/webapps
sudo rm -rf /home/centos/apache-tomcat-9.0.19/webapps/*
echo "DELETING THE ALL OLD BUILD FILES"
else
echo "already latest files are avaliable"
fi

4
if $jobplanet = 'true'
then
cd /home/centos/jenkins-slave-aws/workspace/aws-jobplanet/target
cp admin.war  /home/centos/apache-tomcat-9.0.19/webapps
echo "war files copied to webapps"
else
echo "no files are copied"
fi

5
export JAVA_HOME=/usr/lib/java
export CATALINA_HOME=/home/centos/apache-tomcat-9.0.19
startScript=$CATALINA_HOME/bin/startup.sh

echo "Attempting to start Tomcat via $startScript . . ."
#nohup $startScript &
sudo $startScript

echo "Process complete . . ."
