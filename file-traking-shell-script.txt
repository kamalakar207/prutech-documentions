tomcatPID=$(ps -ef | grep apache-tomcat-9.0.19 | grep -v grep | grep -v restart | awk '{print $2}')

if [ "$tomcatPID" == "" ] 
then
    echo "Tomcat does not appear to be running . . ."
else
    echo "Killing Tomcat using process id of $tomcatPID . . ."
    sudo kill -9 $tomcatPID
    echo "Waiting for process $tomcatPID to end . . ."
    sudo  ps -ef | grep $tomcatPID
    echo "Process $tomcatPID has ended . . ."
fi


for i in /opt/apache-tomcat-9.0.17/webapps/iam.war; do
if $oauth2-iam  = 'true'
then 
cd /opt/apache-tomcat-9.0.17/webapps
sudo tar -cvf ${BUILD_NUMBER}_oauth2-iam.war.tar oauth2-iam.war
sudo mv ${BUILD_NUMBER}_oauth2-iam.war.tar /data/backup/iam_oauth2-iam 
echo "Backup Is Sucessfull"
else
echo "BACK IS TAKEN SUCESSFULLY FOR JOBPLANET"
fi
done

for i in /opt/apache-tomcat-9.0.17/webapps/iam.war; do 
if $oauth2-iam  = 'true'
then
cd /opt/apache-tomcat-9.0.17/webapps
sudo rm -rf /opt/apache-tomcat-9.0.17/webapps/iam_oauth2-iam.war
sudo rm -rf /opt/apache-tomcat-9.0.17/logs/*
echo "DELETING THE ALL OLD BUILD FILES"
else
echo "already latest files are avaliable"
fi
done

for i in /root/.jenkins/workspace/iam_oauth2-iam/target; do
if $oauth2-iam = 'true'
then
cd /root/.jenkins/workspace/iam_oauth2-iam/target
cp iam_oauth2-iam.war  /opt/apache-tomcat-9.0.17/webapps/iam_oauth2-iam.war
echo "war files copied to webapps"
else
echo "no files are copied"
fi
done